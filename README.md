**Stratio Coding Challenge.**


## Task: Shopping Cart 1

Write a program and associated unit tests that can price a basket of goods taking into account some special offers.

The goods that can be purchased, together with their normal prices are:

* Soup - 65p per tin

* Bread - 80p per loaf

* Milk - 1.30 pounds per bottle

* Apples - 1 pound per bag

* Peanuts - 2 euros per bag

* Banana 1.20 euros per bag

Current special offers:

* Apples have a 10% discount off their normal price this week

* Buy 2 tins of soup and get a loaf of bread for half price

The program should accept a list of items in the basket and output the subtotal, the special offer discounts and the fnal price in euros.

Input should be via the command line in the form

~~~~
PriceBasket item1 item2 item3 ...
~~~~

Output should be to the console, for example:

~~~~
Subtotal: 3.10€
Apples 10% off: -0.10€
Total: 3.00€
~~~~

If no special offers are applicable the code should output:

~~~~
Subtotal: 1.30€ (no offers available)
Total price: 1.30€
~~~~

 
---

## Run instructions

* Clone this repository. 

* Navigate to folder stratiocodingchallenge:

~~~~
cd stratiocodingchallenge
~~~~

* This project uses maven. Run the following commands: 

~~~~
$ mvn clean package
$ mvn exec:java
~~~~

* Follow instructions in the terminal. 

## General notes


* To add new shop items, simply add new lines to the following CSV: 

~~~~
stratiocodingchallenge/src/main/resources/items.csv
~~~~

By default, this file accepts "euros" and "pounds" as units of currency. Pounds will be converted to euros automatically when the shop items are loaded to the shop. 


* For the sake of simplicity and lack of specification, discounts can't yet be loaded from a CSV. Instead, new discounts can be added easily by navigating to the file **Utils.java** and created as seen in the method

~~~~
public static ArrayList<Discount> createDiscounts();
~~~~

* Although initially the file **items.csv** contains a field for the **itemId**, I am now using the **productDescription** (name) as the unique identifier for each of the items, and the itemId is not used throughout the code. 


* Item discounts for a certain item are not cumulative. If several discounts can be applied to the same item, only the first of them will be applied. 

