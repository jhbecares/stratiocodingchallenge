package com.stratio.shoppingcart.exceptions;
  
import org.junit.Before;
import org.junit.Test;

import com.stratio.shoppingcart.exceptions.ItemDoesNotExistException;
import com.stratio.shoppingcart.exceptions.ItemsFileNotFoundException;
import com.stratio.shoppingcart.exceptions.ShopEmptyException;
import com.stratio.shoppingcart.shop.ShopItems;
import com.stratio.shoppingcart.utilities.Utils;
 
/**
 * Tests that requesting the price for an item that does not exist 
 * in the shop throws an ItemDoesNotExistException
 *
 */
public class TestItemDoesNotExist {
	
	private String itemsFilename = "testItems.csv";
	private ShopItems shopItems;  

	@Before
	public void loadData() {
		try {
			this.shopItems = Utils.loadShopItems(itemsFilename);
		} catch (ItemsFileNotFoundException e) { 
			e.printStackTrace();
		} catch (ShopEmptyException e) { 
			e.printStackTrace();
		}
	}
	 
	@Test(expected = ItemDoesNotExistException.class)
	public void testItemDoesNotExistException() throws ItemDoesNotExistException {
		String [] items = new String[]{"PriceBasket", "FakeItem"}; // We add two items to the cart
		Utils.readBasket(items, shopItems);  
	}
}
