package com.stratio.shoppingcart.exceptions;
 
import org.junit.Test;

import com.stratio.shoppingcart.exceptions.ItemsFileNotFoundException;
import com.stratio.shoppingcart.exceptions.ShopEmptyException;
import com.stratio.shoppingcart.shop.ShopItems;
import com.stratio.shoppingcart.utilities.Utils;
 

/**
 * Tests that loading an empty shop throws a ShopEmptyException
 *
 */
public class TestContainsItems {
  
	public String emptyShopFilename = "emptyShop.csv";
	public ShopItems shopItems;
	
	@Test(expected = ShopEmptyException.class)
	public void testEmptyShopException() throws ItemsFileNotFoundException, ShopEmptyException {
		this.shopItems = Utils.loadShopItems(emptyShopFilename);
	}
 
}
