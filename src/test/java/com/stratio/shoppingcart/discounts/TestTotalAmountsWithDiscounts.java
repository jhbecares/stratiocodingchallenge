package com.stratio.shoppingcart.discounts;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.stratio.shoppingcart.cart.Basket;
import com.stratio.shoppingcart.discounts.Discount;
import com.stratio.shoppingcart.exceptions.ItemDoesNotExistException;
import com.stratio.shoppingcart.exceptions.ItemsFileNotFoundException;
import com.stratio.shoppingcart.exceptions.ShopEmptyException;
import com.stratio.shoppingcart.shop.ShopItems;
import com.stratio.shoppingcart.utilities.Utils;

/**
 * Tests that all discounts are properly applied when applicable and that 
 * no discounts are applied when not applicable
 *
 */
public class TestTotalAmountsWithDiscounts {
	
	private String itemsFilename = "testItems.csv";
	private ShopItems shopItems;
	private List<Discount> discounts;
	private Basket basket;

	@Before
	public void loadData() {
		try {
			this.shopItems = Utils.loadShopItems(itemsFilename);
		} catch (ItemsFileNotFoundException e) { 
			e.printStackTrace();
		} catch (ShopEmptyException e) { 
			e.printStackTrace();
		}
		this.discounts = Utils.createDiscounts(); 
	}
	
	@Test
	public void testApplesDiscountWhenSingleApplesPurchased() throws ItemDoesNotExistException {
		String [] items = new String[]{"PriceBasket", "Soup","Apples"}; // We add two items to the cart
		basket = Utils.readBasket(items, shopItems);  
		Utils.applyDiscounts(basket, discounts);
		double totalAmount = Utils.calculateTotal(this.basket);
		assertTrue("The total amount for the input data is not correct. Received: " + totalAmount, totalAmount==1.72);
	}

	@Test
	public void testApplesDiscountWhenMultipleApplesPurchased() throws ItemDoesNotExistException {
		String [] items = new String[]{"PriceBasket", "Soup","Apples", "Apples"};  
		basket = Utils.readBasket(items, shopItems);  
		Utils.applyDiscounts(basket, discounts);
		double totalAmount = Utils.calculateTotal(this.basket);
		assertTrue("The total amount for the input data is not correct. Received: " + totalAmount, totalAmount==2.72);
	}
	
	@Test
	public void testBreadDiscountWhenTwoSoupsPurchased() throws ItemDoesNotExistException {
		String [] items = new String[]{"PriceBasket", "Soup","Soup", "Bread"}; 
		basket = Utils.readBasket(items, shopItems);  
		Utils.applyDiscounts(basket, discounts);
		double totalAmount = Utils.calculateTotal(this.basket);
		assertTrue("The total amount for the input data is not correct. Received: " + totalAmount, totalAmount==1.88);
	}
	
	@Test
	public void testSingleBreadDiscountWhenTwoSoupsAndTwoBreadsPurchased() throws ItemDoesNotExistException {
		String [] items = new String[]{"PriceBasket", "Soup","Soup", "Bread", "Bread"}; 
		basket = Utils.readBasket(items, shopItems);  
		Utils.applyDiscounts(basket, discounts);
		double totalAmount = Utils.calculateTotal(this.basket);
		assertTrue("The total amount for the input data is not correct. Received: " + totalAmount, totalAmount==2.77);
	}
	
	@Test
	public void testNoDiscountsWhenNotApplicable() throws ItemDoesNotExistException {
		String [] items = new String[]{"PriceBasket", "Banana","Peanuts", "Milk", "Banana"}; 
		basket = Utils.readBasket(items, shopItems);  
		Utils.applyDiscounts(basket, discounts);
		double totalAmount = Utils.calculateTotal(this.basket);
		assertTrue("The total amount for the input data is not correct. Received: " + totalAmount, totalAmount==5.84);
	}
}
