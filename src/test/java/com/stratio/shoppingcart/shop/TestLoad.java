package com.stratio.shoppingcart.shop;
 
import org.junit.Test;

import com.stratio.shoppingcart.exceptions.ItemsFileNotFoundException;
import com.stratio.shoppingcart.exceptions.ShopEmptyException;
import com.stratio.shoppingcart.shop.ShopItems;
import com.stratio.shoppingcart.utilities.Utils;

/**
 * Tests that items.csv is present AND that we receive an exception
 * when a non-existent filename tries to be loaded. 
 *
 */
public class TestLoad {

	public String itemsFilename = "items.csv";
	public String fakeFilename = "fakefilename.csv";
	public ShopItems shopItems;
	
	@Test(expected = ItemsFileNotFoundException.class)
	public void testLoadFakeFileException() throws ItemsFileNotFoundException, ShopEmptyException {
		this.shopItems = Utils.loadShopItems(fakeFilename); 
	}

	@Test 
	public void testLoadFilenameNoException() throws ItemsFileNotFoundException, ShopEmptyException {
		this.shopItems = Utils.loadShopItems(itemsFilename); 
	}
}
