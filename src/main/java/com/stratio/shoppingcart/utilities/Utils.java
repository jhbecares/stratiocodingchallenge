package com.stratio.shoppingcart.utilities;

import java.util.ArrayList;
import java.util.List;

import com.stratio.shoppingcart.cart.Basket;
import com.stratio.shoppingcart.discounts.Discount;
import com.stratio.shoppingcart.discounts.impl.MforN;
import com.stratio.shoppingcart.discounts.impl.PercentageDiscount;
import com.stratio.shoppingcart.exceptions.ItemDoesNotExistException;
import com.stratio.shoppingcart.exceptions.ItemsFileNotFoundException;
import com.stratio.shoppingcart.exceptions.ShopEmptyException;
import com.stratio.shoppingcart.shop.Product;
import com.stratio.shoppingcart.shop.ShopItems;


public class Utils {

	public static ShopItems loadShopItems(String itemsFilename) throws ItemsFileNotFoundException, ShopEmptyException {
		return new ShopItems(itemsFilename); 
	}

	public static ArrayList<Discount> createDiscounts() {
		ArrayList<Discount> discounts = new ArrayList<Discount>();
		
		// Discount apples
		Discount discountApples = new PercentageDiscount("Apples", 10, "Apples");
		discounts.add(discountApples);
		
		// Discount 2
		String discountName = "Bread";
		List<String> neededItemsToApplyDiscount = new ArrayList<String>();
		neededItemsToApplyDiscount.add("Soup");
		neededItemsToApplyDiscount.add("Soup");
		String discountedItem = "Bread";
		int discountPercentage = 50;
		Discount discountBread = new MforN(discountName, neededItemsToApplyDiscount, discountedItem, discountPercentage);
		discounts.add(discountBread);
		
		return discounts;
	}
	
	public static Basket readBasket(String [] products, ShopItems shopItems) throws ItemDoesNotExistException {
		Basket basket = new Basket();

		for (int i = 1; i < products.length; i++) {
			String productName = products[i];
			Product product = shopItems.getProductByName(productName);
			if (product != null) {
				Product newProd = new Product(product.getProductId(), product.getProductDescription(), product.getPrice(), product.getUnitPrice());
				basket.addProductToBasket(newProd);
			}
		}
		return basket;
	}
	 
	public static double calculateTotal(Basket basket) {
		double price = basket.calculatePrice(); 
		price = (double) Math.round(price * 100) / 100; // only 2 decimal places after rounding the price
		return price;
	}
	
	public static String applyDiscounts(Basket basket, List<Discount> discounts) {
				
		String discountsStr = "";
		for (Discount discount : discounts) {
			String currentDiscount = discount.calculateDiscount(basket);
			if (!currentDiscount.equalsIgnoreCase(""))
				discountsStr += currentDiscount;
		}
		return discountsStr;
	} 
}
