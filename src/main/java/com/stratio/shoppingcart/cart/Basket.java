package com.stratio.shoppingcart.cart;

import java.util.ArrayList;
import java.util.List;

import com.stratio.shoppingcart.shop.Product;


public class Basket {
	private List<Product> products;
	 
	public Basket() {
		products = new ArrayList<Product>();
	}
	
	public Basket(List<Product> products) {
		this.products = products;
	}
  
	public void addProductToBasket(Product product) {
		this.products.add(product);
	}
	
	public double calculatePrice() {
		double totalPrice = 0;
		for (int i = 0; i < this.products.size(); i++) {
			totalPrice += this.products.get(i).getPrice();
		}
		return totalPrice;
	}
	
	public List<Product> getProducts() {
		return this.products;
	}
}
