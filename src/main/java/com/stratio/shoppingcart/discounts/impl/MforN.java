package com.stratio.shoppingcart.discounts.impl;

import java.util.ArrayList;
import java.util.List;

import com.stratio.shoppingcart.cart.Basket;
import com.stratio.shoppingcart.discounts.Discount;
import com.stratio.shoppingcart.shop.Product;

/**
 * We assume that when we have a list of M different items on our basket, we can get item N with a discount 
 * This discount is applicable several times for as long as we have enough items on the cart. 
 *
 */
public class MforN implements Discount {

	private List<String> neededItemsToApplyDiscount;
	private String discountedItem;
	private String discountName;
	private int discountPercentage;
	private double discountedAmount; 
	
	/**
	 * Creates a discount where for a list of different items in the basket, item N gets
	 * discountPercentage% of discount. 
	 * @param discountName, name you give to this discount
	 * @param neededItemsToApplyDiscount, a list of the items needed for the discount to be applicable
	 * @param discountedItem, name of the item to be discounted
	 * @param discountPercentage, percentage discounted from discountedItem
	 */
	public MforN(String discountName, List<String> neededItemsToApplyDiscount, String discountedItem, int discountPercentage) {
		this.setNeededItemsToApplyDiscount(neededItemsToApplyDiscount);
		this.setDiscountName(discountName);
		this.setDiscountedItems(discountedItem);
		this.setDiscountPercentage(discountPercentage);
	}
	 
	@Override
	public String calculateDiscount(Basket basket) {
		
		List<Product> products = basket.getProducts();
		String discounts = "";
		
		// We keep checking for discounts until there's no discount left
		while (checkApplicabilityOfDiscount(basket)) {
			// mark needed items as used
			boolean applied = false;
			for (int j = 0; j < products.size() && !applied; j++) {
				Product prod = products.get(j);
				
				if (prod.getProductDescription().equalsIgnoreCase(discountedItem) && !prod.getDiscount()) {
					// discounted item
					this.discountedAmount = prod.getPrice()*this.discountPercentage/100;
					this.discountedAmount = (double) Math.round(this.discountedAmount * 100) / 100; // only 2 decimal places after rounding the price
					prod.setPrice(prod.getPrice() - this.discountedAmount); 
					prod.setDiscount(true); // we mark that this item is already discounted so
					// noone applies further reductions. We assume items can only be 
					// discounted once
					
					applied = true;
					
					discounts += this.toString() + "\n";
				} 
			}
		}
		
		return discounts;
	}

	@Override
	public boolean checkApplicabilityOfDiscount(Basket basket) {
		// check if all the items 
		List<Product> products = basket.getProducts();
		List<Integer> usedItems = new ArrayList<Integer>();
		
		for (int i = 0; i < neededItemsToApplyDiscount.size(); i++) {
			boolean added = false;
			
			for (int j = 0; j < products.size() && !added; j++) {
				Product prod = products.get(j);
				if (prod.getProductDescription().equalsIgnoreCase(neededItemsToApplyDiscount.get(i)) && 
						!usedItems.contains(j) && !prod.getDiscount()) {
					usedItems.add(j); // we add it to the used items 
					added = true;
				}
			}
			// some of the items are not present in the basket
			if (!added) return false;
		}
		
		
		for (int i = 0; i < usedItems.size(); i++) {
			products.get(usedItems.get(i)).setDiscount(true);
			// we mark this item as already discounted even though it is not true. 
			// we do this because it was already used to apply a discount and we don't want to use it several
			// times for further discounts
		}
		
		return true;
	}

	public String getDiscountName() {
		return discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}

	public List<String> getNeededItemsToApplyDiscount() {
		return neededItemsToApplyDiscount;
	}

	public void setNeededItemsToApplyDiscount(List<String> neededItemsToApplyDiscount) {
		this.neededItemsToApplyDiscount = neededItemsToApplyDiscount;
	}
  
	public String getDiscountedItem() {
		return discountedItem;
	}

	public void setDiscountedItems(String discountedItems) {
		this.discountedItem = discountedItems; 
	}

	public int getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(int discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String toString() {
		String str = "";
		str += this.discountName + " " + this.discountPercentage + "% off: -" + this.discountedAmount + "euros";
		return str;
	}
}
