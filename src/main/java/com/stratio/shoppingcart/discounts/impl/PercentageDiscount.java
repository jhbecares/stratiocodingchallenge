package com.stratio.shoppingcart.discounts.impl;

import java.util.List;

import com.stratio.shoppingcart.cart.Basket;
import com.stratio.shoppingcart.discounts.Discount;
import com.stratio.shoppingcart.shop.Product;


public class PercentageDiscount implements Discount {

	private String discountedProductDescription;
	private int discountPercentage;
	private String discountName;
	private double discountedAmount = 0; 
	
	/**
	 * Creates a new percentage discount. That is, for a given product, 
	 * discountPercentage% of discount. 
	 * @param discountedProductDescription name of product to be discounted
	 * @param discountPercentage percentage of discount to apply
	 * @param discountName  
	 */
	public PercentageDiscount(String discountedProductDescription, int discountPercentage, String discountName) {
		this.discountedProductDescription = discountedProductDescription;
		this.discountPercentage = discountPercentage;
		this.setDiscountName(discountName);
	}
	
	@Override
	public String calculateDiscount(Basket basket) {
		String discounts = "";
		
		while (checkApplicabilityOfDiscount(basket)) {
			// if the product we want to discount is in our basket
			for (int i = 0; i < basket.getProducts().size(); i++) {
				Product prod = basket.getProducts().get(i);
				if (prod.getProductDescription().equalsIgnoreCase(discountedProductDescription)) {
					// we apply the discount percentage to each of the products with the same name
					this.discountedAmount = prod.getPrice()*discountPercentage/100;
					this.discountedAmount = (double) Math.round(this.discountedAmount * 100) / 100; // only 2 decimal places after rounding the price
					prod.setPrice(prod.getPrice() - this.discountedAmount); 
					
					prod.setDiscount(true); // we mark that this item is already discounted so
					// noone applies further reductions. We assume items can only be 
					// discounted once
					
					discounts += this.toString() + "\n";
				}
			}
		}
		
		return discounts;
	}

	@Override
	public boolean checkApplicabilityOfDiscount(Basket basket) {
		List<Product> products = basket.getProducts();

		for (Product product : products) {
			// if any of the products in our basket matches the discounted product, 
			// then we return true because we can apply this discount to our basket
			if (product.getProductDescription().equalsIgnoreCase(discountedProductDescription)
					&& !product.getDiscount()) return true;
		}
		
		return false;
	}

	public String getDiscountName() {
		return discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}

	public String toString() {
		String str = "";
		str += this.discountName + " " + this.discountPercentage + "% off: -" + this.discountedAmount + "euros";
		return str;
	}
	
}
