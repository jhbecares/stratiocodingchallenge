package com.stratio.shoppingcart.discounts;

import com.stratio.shoppingcart.cart.Basket;

public interface Discount {

	/**
	 * Checks if the given discount can be applies and updates the basket items
	 * when necessary. 
	 * @param basket
	 * @return Printable string with the discount characteristics. 
	 * Example: Apples 10% off: -0.11 euros
	 */
	public String calculateDiscount(Basket basket);
	
	/**
	 * Checks the applicability of the current discount for a given basket
	 * @param basket
	 * @return True if the current discount can be applied to the basket
	 */
	public boolean checkApplicabilityOfDiscount(Basket basket);
		
}
