package com.stratio.shoppingcart.shop;

public class Product {

	private int productId;
	private String productDescription; // we assume the product description is unique, as if it was the product id. 
	// this is because the queries will be done on a description basis, so we assume they cannot be duplicated. 
	private double price;
	private String unitPrice;
	private Boolean discounted;
	
	public Product(int productId, String productDescription, double price, String unitPrice) {
		this.setProductId(productId);
		this.setProductDescription(productDescription);
		this.setPrice(price);
		this.setUnitPrice(unitPrice);
		this.setDiscount(false); // by default, no items are discounted
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Boolean getDiscount() {
		return discounted;
	}

	public void setDiscount(Boolean discount) {
		this.discounted = discount;
	}
	
	public String toString() {
		String str = "";
		str += "ProductId=" + this.productId + ";";
		str += "ProductDescription=" + this.productDescription + ";";
		str += "Price=" + this.price + this.unitPrice;
		return str;
	}
	
	public String shortToString() {
		String str = "";
		str += "Product=" + this.productDescription + ";";
		str += "Price=" + this.price + this.unitPrice;
		return str;
	}
}
