package com.stratio.shoppingcart.shop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.stratio.shoppingcart.exceptions.ItemDoesNotExistException;
import com.stratio.shoppingcart.exceptions.ItemsFileNotFoundException;
import com.stratio.shoppingcart.exceptions.ShopEmptyException;

 
public class ShopItems {

	private String filename = "main/resources/items.csv";
	private String resourcesPath = "main/resources/";
	private List<Product> products;
	private double conversionRate = 1/0.9; // conversion rate from pounds to euros
	
	/**
	 * Loads products from the default CSV file, "items.csv",
	 * placed under the resources folder. 
	 * @throws ItemsFileNotFoundException 
	 */
	public ShopItems() throws ItemsFileNotFoundException {
		this.setProducts(new ArrayList<Product>());
		setProducts(this.loadItemsFromCSV());
	}
	
	/**
	 * Loads products from a CSV file under the resources 
	 * folder and with name filename. 
	 * @param filename
	 * @throws ItemsFileNotFoundException 
	 * @throws ShopEmptyException 
	 */
	public ShopItems(String filename) throws ItemsFileNotFoundException, ShopEmptyException {
		this.setProducts(new ArrayList<Product>());
		
		setProducts(this.loadItemsFromCSV(filename));	
		if (this.products.size() == 0) {
			throw new ShopEmptyException("No items in the shop");
		}
	}
	
	public ShopItems(ArrayList<Product> products) {
		this.setProducts(products);
	}
	
	
	private List<Product> loadItemsFromCSV() throws ItemsFileNotFoundException {
		// Load items from default filename
		List<Product> products = this.readCSV(this.filename);
		return products;
	}
	
	private List<Product> loadItemsFromCSV(String filename) throws ItemsFileNotFoundException {
		// Load items from personalized CSV
		List<Product> products = this.readCSV(this.resourcesPath + filename);
		return products;
	}

	/**
	 * Creates a list of the shop products based on the items described
	 * under the file "resources/filename". 
	 * @param fileName
	 * @return List of the products
	 * @throws ItemsFileNotFoundException 
	 */
	private List<Product> readCSV(String fileName) throws ItemsFileNotFoundException {
		List<Product> products = new ArrayList<Product>();
		 
		//Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		
		File file;
		try { 
			file = new File(classLoader.getResource(fileName).getFile());
		} catch(NullPointerException e) {
			throw new ItemsFileNotFoundException("Couldn't find file with name " + fileName);
		}

		try (Scanner scanner = new Scanner(file)) {

			scanner.nextLine(); // we skip the row with labels
			
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				
				String[] lineSplit = line.split(";");

				int productId = Integer.parseInt(lineSplit[0]);
				String productDescription = lineSplit[1];
				double price = Double.parseDouble(lineSplit[2]);;
				String unitPrice = lineSplit[3];
				
				
				if (unitPrice.equalsIgnoreCase("pounds")) {
					price = price * this.conversionRate;
					price = (double) Math.round(price * 100) / 100; // only 2 decimal places after rounding the price
					unitPrice = "euros";
				}
				
				Product currentProd = new Product(productId, productDescription, price, unitPrice);
				products.add(currentProd);
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return products; 
	}
	  
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
		
	public Product getProductById(int productId) {
		for (Product product : this.products) {
			if (product.getProductId() == productId) {
				return product;
			}
		}
		return null;
	}
	
	public Product getProductByName(String productName) throws ItemDoesNotExistException {
		for (Product product : this.products) {
			if (product.getProductDescription().equalsIgnoreCase(productName)) {
				return product;
			}
		}
		throw new ItemDoesNotExistException("The following item does not exist in the shop: " + productName);
	}
	
	public boolean checkIfProductExists(Product product) {
		for (Product prod : this.products) {
			if (prod.equals(product)) return true;
		}
		return false;
	}
	
	public String toString() {
		String str = "Shop items: \n";
		for (Product product : products) {
			str += product.shortToString() + "\n";
		}
		return str;
	}
}
