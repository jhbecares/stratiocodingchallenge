package com.stratio.shoppingcart.shop; 
 

import java.util.List;
import java.util.Scanner;

import com.stratio.shoppingcart.cart.Basket;
import com.stratio.shoppingcart.discounts.Discount;
import com.stratio.shoppingcart.exceptions.ItemDoesNotExistException;
import com.stratio.shoppingcart.exceptions.ItemsFileNotFoundException;
import com.stratio.shoppingcart.exceptions.ShopEmptyException;
import com.stratio.shoppingcart.utilities.Utils;

public class Main {

	public static String itemsFilename = "items.csv";
 
	public static void main(String[] args) { 
		   		
		Scanner reader = new Scanner(System.in);  // Reading from System.in 
		// We load shop items to the shop. All resources need to be placed
		// under the path src/resources for it to work. 
		ShopItems shopItems = null;
		try {
			shopItems = Utils.loadShopItems(itemsFilename);
			System.out.println(itemsFilename + " loaded.\n" + shopItems.toString());
		} catch (ItemsFileNotFoundException e) {
			e.printStackTrace();
			// log error
		} catch (ShopEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// log error
		}
		
		boolean exit = false;
		while(!exit) {  
			
			
			// We load discount items to the shop. Right now they are just created right here
			// but they could easily work in a CSV fashion in the future
			List<Discount> discounts = Utils.createDiscounts(); 
			
			System.out.println("Introduce your price basket in the format: \"PriceBasket element1 element2\"");
			System.out.println("Otherwise, type \"exit\" to end the program.");

			if (reader.hasNextLine()) {
				String line = reader.nextLine();
				String[] splitLine = line.split(" ");
				  
				if (splitLine[0].equalsIgnoreCase("PriceBasket")) {					
					// We read the basket from the standard input
					Basket basket = null;
					try {
						basket = Utils.readBasket(splitLine, shopItems);
					} catch (ItemDoesNotExistException e) { 
						e.printStackTrace();
					}  
					
					// We calculate and print the total price of the basket before discounts
					double totalAmount = Utils.calculateTotal(basket);
					System.out.println("Subtotal: " + totalAmount + " euros");
					
					// We apply the applicable discounts to our basket
					String discountStr = Utils.applyDiscounts(basket, discounts);
					if (discountStr.equalsIgnoreCase("")) {
						System.out.println("(No offers applicable.)");
					} else {
						System.out.print(discountStr);
					}
					
					
					// We calculate the final price after discounts
					double finalAmount = Utils.calculateTotal(basket);
					System.out.println("Total: " + finalAmount + " euros\n");
					
				} else if (splitLine[0].equalsIgnoreCase("Exit")) {
					exit = true;
				} 
			}
		}
		reader.close(); 
	}
  
}
