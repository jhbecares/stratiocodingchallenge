package com.stratio.shoppingcart.exceptions;

public class ItemDoesNotExistException extends Exception {

	public ItemDoesNotExistException() { super(); }
	public ItemDoesNotExistException(String message) { super(message); }
	public ItemDoesNotExistException(String message, Throwable cause) { super(message, cause); }
	public ItemDoesNotExistException(Throwable cause) { super(cause); }
}
