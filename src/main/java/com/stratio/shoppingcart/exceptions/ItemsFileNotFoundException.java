package com.stratio.shoppingcart.exceptions;

public class ItemsFileNotFoundException extends Exception {

	public ItemsFileNotFoundException() { super(); }
	public ItemsFileNotFoundException(String message) { super(message); }
	public ItemsFileNotFoundException(String message, Throwable cause) { super(message, cause); }
	public ItemsFileNotFoundException(Throwable cause) { super(cause); }
}
