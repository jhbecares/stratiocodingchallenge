package com.stratio.shoppingcart.exceptions;

public class ShopEmptyException extends Exception {

	public ShopEmptyException() { super(); }
	public ShopEmptyException(String message) { super(message); }
	public ShopEmptyException(String message, Throwable cause) { super(message, cause); }
	public ShopEmptyException(Throwable cause) { super(cause); }
}
